import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:media_app/data/remote/tv_shows_remote_service.dart';
import 'package:media_app/data/repository/tv_shows_repository_impl.dart';
import 'package:media_app/data/usecase/get_tv_shows_by_id_use_case_impl.dart';
import 'package:media_app/data/usecase/get_tv_shows_posters_use_case_impl.dart';
import 'package:media_app/domain/repository/tv_shows_repository.dart';
import 'package:media_app/domain/usecases/tv_shows/get_tv_shows_by_id_use_case.dart';
import 'package:media_app/domain/usecases/tv_shows/get_tv_shows_posters_use_case.dart';
import 'package:media_app/presentation/screens/auth/auth_screen_view_model.dart';
import 'package:media_app/presentation/screens/tv_shows/tv_shows_viewmodel.dart';
import 'package:media_app/presentation/screens/video_info/video_screen_view_model.dart';
import 'package:media_app/presentation/widgets/app_widget.dart';
import 'package:media_app/utils/constants.dart';
import 'package:media_app/utils/logging_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:provider/provider.dart';

final getIt = GetIt.I;

void main() {
  _setupDioClient();
  _setupSystemChromeSettings();
  _registerSingletones();
  _registerProviders();
  // MultiProvider(providers: [
  //   Provider<TvShowsViewModel>(create: (_) => TvShowsViewModel(),),
  //   Provider<AuthScreenViewModel>(create: (_) => AuthScreenViewModel(),),
  //   Provider<VideoScreenViewModel>(create: (_) => VideoScreenViewModel(),),
  // ]);
  runApp(AppWidget(),);
}

void _setupDioClient() {
  final dio = Dio();
  dio.interceptors.add(LoggingInterceptor());
  dio.options.headers["Content-Type"] = "application/vnd.api+json";
  dio.options.headers["Accept"] = "application/json, text/plain, */*";
  dio.options.responseType = ResponseType.json;

  getIt.registerLazySingleton<Dio>(() => dio);
}

void  _setupSystemChromeSettings() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: PRIMARY_COLOR,
      systemNavigationBarIconBrightness: Brightness.light,
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.light
  ));
}

void _registerProviders() {
  getIt.registerLazySingleton(() => TvShowsViewModel());
  getIt.registerLazySingleton(() => AuthScreenViewModel());
  getIt.registerLazySingleton(() => VideoScreenViewModel());
}

void _registerSingletones() {
  getIt.registerLazySingleton<TvShowsRemoteService>(() => TvShowsRemoteService());
  getIt.registerLazySingleton<TvShowsRepository>(() => TvShowsRepositoryImpl());
  getIt.registerLazySingleton<GetTvShowsPostersUseCase>(() => GetTvShowsPostersUseCaseImpl());
  getIt.registerLazySingleton<GetTvShowByIdUseCase>(() => GetTvShowByIdUseCaseImpl());
}
