import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class PlayerControls extends StatefulWidget {
  final VideoPlayerController controller;

  PlayerControls({Key key, this.controller}) : super(key: key);

  @override
  _PlayerControlsState createState() => _PlayerControlsState();
}

class _PlayerControlsState extends State<PlayerControls> {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Stack(
      children: [
        IconButton(
            icon: Icon(
              widget.controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
              color: Theme.of(context).accentColor,
            ),
            onPressed: () {
              widget.controller.value.isPlaying ? widget.controller.pause() : widget.controller.play();
              setState(() {

              });
            })
      ],
    ));
  }
}
