import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/components/button/gf_button.dart';
import 'package:getwidget/components/button/gf_icon_button.dart';
import 'package:getwidget/size/gf_size.dart';
import 'package:media_app/presentation/widgets/text_input.dart';

class RegisterWidget extends StatelessWidget {
  final Function signInClicked;

  const RegisterWidget({this.signInClicked});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextInput(
                hintText: 'Email',
              ),
              SizedBox(
                height: 16,
              ),
              TextInput(
                hintText: 'Password',
              ),
              SizedBox(
                height: 16,
              ),
              TextInput(
                hintText: 'Confirm password',
              ),
              SizedBox(
                height: 16,
              ),
              GFButton(
                fullWidthButton: true,
                onPressed: () {},
                color: Theme.of(context).accentColor,
                text: 'Sign up',
                size: GFSize.LARGE,
              ),
              SizedBox(
                height: 8,
              ),
              RichText(
                text: TextSpan(
                    style: TextStyle(color: Colors.white.withOpacity(0.5)),
                    children: [
                      TextSpan(text: 'Already have an account? '),
                      TextSpan(
                          text: 'Sign In',
                          style:
                              TextStyle(color: Theme.of(context).accentColor),
                          recognizer: TapGestureRecognizer()
                            ..onTap = signInClicked),
                    ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
