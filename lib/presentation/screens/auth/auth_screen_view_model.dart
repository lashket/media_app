import 'package:flutter/material.dart';

enum ScreenState {
  REGISTER,
  LOGIN
}

class AuthScreenViewModel with ChangeNotifier {

  ScreenState _screenState = ScreenState.LOGIN;
  get screenState  => _screenState;

  void showRegisterView() {
    _screenState = ScreenState.REGISTER;
    notifyListeners();
  }

  void showLoginView() {
    _screenState = ScreenState.LOGIN;
    notifyListeners();
  }

}