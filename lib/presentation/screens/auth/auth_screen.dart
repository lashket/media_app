import 'package:flutter/material.dart';
import 'package:media_app/presentation/screens/auth/auth_screen_view_model.dart';
import 'package:media_app/presentation/screens/auth/login_widget.dart';
import 'package:media_app/presentation/screens/auth/register_widget.dart';
import 'package:media_app/presentation/widgets/provider_widget.dart';
import 'package:provider/provider.dart';

class AuthScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProviderWidget<AuthScreenViewModel>(
      builder: (context, provider, child) =>
          provider.screenState == ScreenState.LOGIN
              ? LoginWidget(
                  signUpClickListener: () {
                    provider.showRegisterView();
                  },
                )
              : RegisterWidget(
                  signInClicked: () => provider.showLoginView(),
                ),
    );
  }
}
