import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/components/button/gf_button.dart';
import 'package:getwidget/components/button/gf_icon_button.dart';
import 'package:getwidget/size/gf_size.dart';
import 'package:media_app/presentation/widgets/text_input.dart';

class LoginWidget extends StatelessWidget {
  final Function signUpClickListener;

  const LoginWidget({this.signUpClickListener});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextInput(
                hintText: 'Email',
              ),
              SizedBox(
                height: 16,
              ),
              TextInput(
                hintText: 'Password',
              ),
              SizedBox(
                height: 16,
              ),
              GFButton(
                fullWidthButton: true,
                onPressed: () {},
                color: Theme.of(context).accentColor,
                text: 'Sign in',
                size: GFSize.LARGE,
              ),
              SizedBox(
                height: 8,
              ),
              RichText(
                text: TextSpan(
                    style: TextStyle(color: Colors.white.withOpacity(0.5)),
                    children: [
                      TextSpan(text: 'Don\'t have an account yet? '),
                      TextSpan(
                          text: 'Sign up',
                          style:
                              TextStyle(color: Theme.of(context).accentColor),
                          recognizer: TapGestureRecognizer()
                            ..onTap = signUpClickListener),
                    ]),
              ),
              SizedBox(
                height: 24,
              ),
              Text(
                'Or continue with:',
                style: TextStyle(color: Colors.white.withOpacity(0.5)),
              ),
              SizedBox(
                height: 8,
              ),
              _socialLinks()
            ],
          ),
        ),
      ),
    );
  }

  Widget _socialLinks() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GFIconButton(
          color: Color(0xff4267B2),
          onPressed: () {},
          size: GFSize.MEDIUM,
          icon: Image.asset('assets/img/ic_facebook.png'),
        ),
        SizedBox(
          width: 16,
        ),
        GFIconButton(
          color: Colors.red,
          onPressed: () {},
          size: GFSize.MEDIUM,
          icon: Image.asset('assets/img/ic_google.png'),
        ),
        SizedBox(
          width: 16,
        ),
        GFIconButton(
          color: Color(0xff1DA1F2),
          onPressed: () {},
          size: GFSize.MEDIUM,
          icon: Image.asset('assets/img/ic_twitter.png'),
        ),
      ],
    );
  }
}
