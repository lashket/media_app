import 'package:flutter/material.dart';
import 'package:media_app/presentation/screens/tv_channels/tv_channel_grid_item_view.dart';
import 'package:media_app/presentation/screens/tv_channels/tv_channel_section_view.dart';

class TvChannels extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      child: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              centerTitle: true,
              title: Text('Channels'),
            ),
            SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
              ),
              delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  return TcChannelGridItemView();
                },
                childCount: 8,
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  return TvChannelSectionView('$index section');
                },
                childCount: 10,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
