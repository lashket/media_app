import 'package:flutter/material.dart';

class TvChannelListItemView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Container(
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(4)),
          child: Image.asset('assets/img/tv_logo.png'),
        ),
      ),
    );
  }
}
