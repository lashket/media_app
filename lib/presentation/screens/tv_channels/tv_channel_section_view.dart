import 'package:flutter/material.dart';
import 'package:media_app/domain/models/tv_shows_category.dart';
import 'package:media_app/presentation/screens/tv_channels/tv_channel_list_item_view.dart';
import 'package:media_app/presentation/widgets/video/sections/genre_section_item_view.dart';

class TvChannelSectionView extends StatefulWidget {

  final String title;

  const TvChannelSectionView(this.title);

  @override
  _TvChannelSectionViewState createState() => _TvChannelSectionViewState();
}

class _TvChannelSectionViewState extends State<TvChannelSectionView> {

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 8, bottom: 8, left: 16),
          child: Text(
            widget.title,
            style: TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.w500),
//          style: Theme.of(context).textTheme.headline1,
          ),
        ),
        Container(
          height: 100,
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: 15,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: TvChannelListItemView(),
              );
            },
          ),
        )
      ],
    );
  }
}
