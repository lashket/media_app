import 'package:flutter/material.dart';

class TcChannelGridItemView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(4)),
          child: Container(
            color: Colors.white,
            child: Image.asset('assets/img/tv_logo.png'),
          ),
        ),
      ),
    );
  }
}
