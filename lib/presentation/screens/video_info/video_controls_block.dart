import 'package:flutter/material.dart';
import 'package:media_app/domain/entities/video.dart';
import 'package:media_app/presentation/screens/video_info/video_details_screen.dart';
import 'package:media_app/presentation/widgets/button_with_subtitle.dart';
import 'package:media_app/utils/constants.dart';
import 'package:share/share.dart';

class VideoControls extends StatelessWidget {

  final Video videoDetails;

  VideoControls({this.videoDetails});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white.withOpacity(0.05),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12),
        child: Row(
          children: [
            Expanded(
                child: ButtonWithSubtitle(
                    text: 'watchlist',
                    icon: Icon(
                      Icons.add_circle_outline,
                      color: SECONDARY_TEXT_COLOR,
                    ),
                    onTap: () {
                      print('watch list button clicked');
                    },)),
            Expanded(
                child: ButtonWithSubtitle(
                    text: 'share',
                    icon: Icon(
                      Icons.share,
                      color: SECONDARY_TEXT_COLOR,
                    ),
                    onTap: () {
                      Share.share('See ${videoDetails.name} on Media App\nhttp://kalco.me/video/${videoDetails.id}');
                    })),
            Expanded(
                child: ButtonWithSubtitle(
                    text: 'download',
                    icon: Icon(
                      Icons.download_rounded,
                      color: SECONDARY_TEXT_COLOR,
                    ),
                    onTap: () {
                      print('download button clicked');
                    },)),
          ],
        ),
      ),
    );
  }

}
