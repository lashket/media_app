import 'package:flutter/material.dart';

class VideoDetailsHeader extends StatelessWidget {
  final String name;
  final String photoUrl;

  VideoDetailsHeader({this.name, this.photoUrl});

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      expandedHeight: MediaQuery.of(context).size.height / 3,
      floating: false,
      pinned: true,
      stretch: true,
      brightness: Brightness.dark,
      actions: [
        Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: InkWell(
              onTap: () {},
              child: Icon(
                Icons.favorite_outline_sharp,
                size: 26.0,
              ),
            )),
      ],
      flexibleSpace: FlexibleSpaceBar(
          stretchModes: [StretchMode.zoomBackground, StretchMode.fadeTitle],
          centerTitle: true,
          title: Text(name,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
              )),
          background: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(photoUrl),
            )),
            child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [
                    Theme.of(context).primaryColor,
                    Colors.transparent
                  ])),
            ),
          )),
    );
  }
}
