import 'package:flutter/material.dart';
import 'package:media_app/domain/entities/video.dart';
import 'package:media_app/navigation/routes.dart';
import 'package:media_app/presentation/screens/player/player_screen.dart';
import 'package:media_app/presentation/screens/video_info/seasons_header.dart';
import 'package:media_app/presentation/screens/video_info/video_controls_block.dart';
import 'package:media_app/presentation/screens/video_info/video_screen_view_model.dart';
import 'package:media_app/presentation/widgets/provider_widget.dart';
import 'package:media_app/presentation/widgets/rating_icon.dart';
import 'package:media_app/presentation/widgets/video/sections/video_section_view.dart';
import 'package:media_app/utils/constants.dart';

class VideoBody extends StatefulWidget {
  @override
  _VideoBodyState createState() => _VideoBodyState();
}

class _VideoBodyState extends State<VideoBody> with TickerProviderStateMixin {
  bool _isDataLoaded = false;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        _isDataLoaded = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: _isDataLoaded ? 1.0 : 0.0,
      duration: Duration(milliseconds: 500),
      child: ProviderWidget<VideoScreenViewModel>(
        builder: (context, provider, child) => CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Center(child: _movieDescription(context, provider.video)),
            ),
            SliverToBoxAdapter(
              child: SeasonsHeader(),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return _episodeListItem(index, context);
                },
                childCount: 10,
              ),
            ),
            SliverToBoxAdapter(
              child: Center(
                  child: VideoSectionView(
                title: 'Similar',
              )),
            ),
          ],
        ),
      ),
    );
  }

  Widget _movieDescription(BuildContext context, Video video) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 16),
          child: _ratingSeasonsYearRow(context),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child: Text(
            'comedy, fantastic, drama',
            style: Theme.of(context).textTheme.subtitle2,
          ),
        ),
        SizedBox(
          height: 8,
        ),
        VideoControls(
          videoDetails: video,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
          child: Text(
            video.description,
            style: Theme.of(context).textTheme.subtitle2,
          ),
        ),
      ],
    );
  }

  Widget _ratingSeasonsYearRow(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        children: [
          Container(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                RatingIcon(
                  true,
                  EdgeInsets.only(right: 2),
                  starSize: 18,
                ),
                RatingIcon(
                  true,
                  EdgeInsets.all(2),
                  starSize: 18,
                ),
                RatingIcon(
                  true,
                  EdgeInsets.all(2),
                  starSize: 18,
                ),
                RatingIcon(
                  true,
                  EdgeInsets.all(2),
                  starSize: 18,
                ),
                RatingIcon(
                  false,
                  EdgeInsets.all(2),
                  starSize: 18,
                ),
              ],
            ),
          ),
          SizedBox(
            width: 2,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 4),
            child: VerticalDivider(
              color: SECONDARY_TEXT_COLOR,
            ),
          ),
          SizedBox(
            width: 4,
          ),
          Text(
            '4 seasons',
            style: Theme.of(context).textTheme.subtitle2,
          ),
          SizedBox(
            width: 2,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 4),
            child: VerticalDivider(
              color: SECONDARY_TEXT_COLOR,
            ),
          ),
          SizedBox(
            width: 2,
          ),
          Text(
            '2012',
            style: Theme.of(context).textTheme.subtitle2,
          ),
        ],
      ),
    );
  }

  Widget _episodeListItem(int position, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(FadeInRoute(widget: PlayerScreen()));
      },
      child: Container(
        color: position % 2 != 0
            ? Colors.white.withOpacity(0.05)
            : Colors.white.withOpacity(0.1),
        child: Row(
          children: [
            SizedBox(
              width: 16,
            ),
            Container(
              width: 50,
              height: 50,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4.0),
                child: Image.network(
                  'https://sm.ign.com/ign_ru/screenshot/default/stranger-things-3-recensione_rack.jpg',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              width: 16,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '1 episode',
                    style: LIST_TITLE_STYLE,
                  ),
                  Text(
                    'NewStudio, Кураж Бомбей',
                    style: LIST_SUBTITLE_STYLE,
                  )
                ],
              ),
            ),
            Container(
              width: 70,
              height: 70,
              child: Icon(
                Icons.file_download,
                color: SECONDARY_TEXT_COLOR,
              ),
            )
          ],
        ),
      ),
    );
  }
}
