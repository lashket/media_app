import 'package:flutter/material.dart';
import 'package:media_app/domain/entities/season.dart';
import 'package:media_app/utils/constants.dart';

class SeasonsHeader extends StatelessWidget {

  final List<Season> seasons;
  final Function(Season season) onSeasonSelected;

  SeasonsHeader({this.seasons, this.onSeasonSelected});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white.withOpacity(0.05),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12),
        child: Row(
          children: [
            SizedBox(width: 16,),
            Text('1 season', style: TextStyle(color: PRIMARY_TEXT_COLOR,  fontSize: 14, fontWeight: FontWeight.bold),),
            Icon(Icons.arrow_drop_down_sharp, color: PRIMARY_TEXT_COLOR,)
          ],
        ),
      ),
    );
  }


}
