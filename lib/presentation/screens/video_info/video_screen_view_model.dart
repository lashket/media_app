import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:media_app/domain/entities/video.dart';
import 'package:media_app/domain/usecases/tv_shows/get_tv_shows_by_id_use_case.dart';

enum Status { LOADING, LOADED, ERROR }

class VideoScreenViewModel with ChangeNotifier {
  final GetTvShowByIdUseCase getTvShowByIdUseCase =
      GetIt.I.get<GetTvShowByIdUseCase>();

  Video _video;

  Video get video => _video;

  Status _status = Status.LOADING;

  Status get status => _status;

  void fetchVideoById(int id) async {
    if (id == null) {
      _status = Status.ERROR;
      notifyListeners();
      return;
    }
    _status = Status.LOADING;
    notifyListeners();
    final response = await getTvShowByIdUseCase.fetchVideoById(id);
    if (response.isSuccess) {
      _video = response.data;
      _status = Status.LOADED;
      notifyListeners();
    }
  }
}
