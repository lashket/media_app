import 'package:flutter/material.dart';

import 'package:media_app/presentation/screens/video_info/header.dart';
import 'package:media_app/presentation/screens/video_info/video_body.dart';
import 'package:media_app/presentation/screens/video_info/video_screen_view_model.dart';
import 'package:media_app/presentation/widgets/error_view.dart';
import 'package:media_app/presentation/widgets/provider_widget.dart';
import 'package:provider/provider.dart';

class VideoDetails extends StatelessWidget {

  final int videoId;

  VideoDetails({@required this.videoId});

  @override
  Widget build(BuildContext context) {
    return  ProviderWidget<VideoScreenViewModel>(
      onProviderReady: (provider) {
        provider.fetchVideoById(videoId);
      },
      builder:(context, provider, child) => Scaffold(
          backgroundColor: Theme.of(context).primaryColor,
          body: _getBodyAccordingStatus(provider.status, context, provider),
      ),
    );
  }
  
  Widget _getBodyAccordingStatus(Status status, BuildContext context, VideoScreenViewModel provider) {
    switch(status) {
      case Status.LOADING: return Center(child: CircularProgressIndicator());
      case Status.LOADED: return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: NestedScrollView(
          physics: BouncingScrollPhysics(),
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              VideoDetailsHeader(
                name: provider.video.name,
                photoUrl: provider.video.posterUrl,
              ),
            ];
          },
          body: VideoBody(),
        ),
      );
      case Status.ERROR: return ErrorView();
    }
    return Container();
  }
  
}