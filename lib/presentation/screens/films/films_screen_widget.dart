import 'package:flutter/material.dart';
import 'package:media_app/presentation/widgets/video/header/video_header_page_view.dart';
import 'package:media_app/presentation/widgets/video/sections/video_sections_widget.dart';

class FilmsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      color: Theme.of(context).primaryColor,
      child: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              // VideoHeaderPageView([]),
              VideoSectionsWidget()
            ],
          ),
        ),
      ),
    );
  }

}
