import 'package:flutter/material.dart';
import 'package:media_app/presentation/screens/tv_shows/tv_shows_viewmodel.dart';
import 'package:media_app/presentation/widgets/orientation_layout.dart';
import 'package:media_app/presentation/widgets/provider_widget.dart';
import 'package:media_app/presentation/widgets/responsive_layout.dart';
import 'package:media_app/presentation/widgets/video/header/video_header_page_view.dart';
import 'package:media_app/presentation/widgets/video/sections/video_sections_widget.dart';
import 'package:provider/provider.dart';

class TvShows extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProviderWidget<TvShowsViewModel>(
      onProviderReady: (provider) {
        provider.fetchTvShows();
      },
      builder: (context, provider, child) => Container(
        height: double.infinity,
        color: Theme.of(context).primaryColor,
        child: provider.status == Status.LOADED
            ? OrientationLayout(
                landscape: Row(
                  children: [
                    Expanded(child: VideoHeaderPageView(provider.posters)),
                    Expanded(
                        child:
                            SingleChildScrollView(child: VideoSectionsWidget()))
                  ],
                ),
                portrait: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                          height: MediaQuery.of(context).size.height / 2,
                          child: VideoHeaderPageView(provider.posters)),
                      VideoSectionsWidget()
                    ],
                  ),
                ),
              )
            : _loading(),
      ),
    );
  }

  Widget _loading() => Center(
        child: CircularProgressIndicator(),
      );
}
