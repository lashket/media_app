import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:media_app/domain/entities/video.dart';
import 'package:media_app/domain/usecases/tv_shows/get_tv_shows_posters_use_case.dart';

enum Status {
  LOADING,
  LOADED
}

class TvShowsViewModel with ChangeNotifier {

  GetTvShowsPostersUseCase getTvShowsPostersUseCase = GetIt.I.get<GetTvShowsPostersUseCase>();

  Status _status = Status.LOADING;
  Status get status => _status;

  List<Video> _posters = [];
  List<Video> get posters => _posters;

  void fetchTvShows() async {
    final videosResponse = await getTvShowsPostersUseCase.execute();
    if(videosResponse.isSuccess) {
      _posters = videosResponse.data;
    }
    _status = Status.LOADED;
    notifyListeners();
  }

}