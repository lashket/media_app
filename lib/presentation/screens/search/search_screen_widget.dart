import 'package:flutter/material.dart';
import 'package:media_app/presentation/screens/search/search_input.dart';

import 'filter_bottom_sheet.dart';

class SearchScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
              context: context,
              builder: (BuildContext ctx) {
                return FilterBottomSheet();
              });
        },
        child: Icon(Icons.filter_alt),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: SearchInput(),
            )
          ],
        ),
      )
    );
  }
}
