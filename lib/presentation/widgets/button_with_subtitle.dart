import 'package:flutter/material.dart';

class ButtonWithSubtitle extends StatelessWidget {
  final String text;
  final Icon icon;
  final VoidCallback onTap;

  ButtonWithSubtitle({Key key, this.text, this.icon, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap.call();
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          icon,
          SizedBox(
            height: 4,
          ),
          Text(
            text,
            style: Theme.of(context).textTheme.subtitle2,
          )
        ],
      ),
    );
  }
}