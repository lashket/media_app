import 'package:flutter/material.dart';
import 'package:media_app/domain/entities/video.dart';

class VideoHeaderPage extends StatelessWidget {

  final Video video;

  const VideoHeaderPage({this.video}) : assert(video != null);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          child: Image.network(video.posterUrl, fit: BoxFit.cover,),
        ),
        Positioned.fill(child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [
                  Theme.of(context).primaryColor,
                  Colors.transparent
                ]
              )
            ),
            width: double.infinity,
            height: 150,

          ),
        ))
      ],
    );
  }
}
