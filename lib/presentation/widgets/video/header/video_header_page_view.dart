import 'package:flutter/material.dart';
import 'package:media_app/domain/entities/video.dart';
import 'package:media_app/navigation/routes.dart';
import 'package:media_app/presentation/screens/video_info/video_details_screen.dart';
import 'package:media_app/presentation/widgets/video/header/video_header_page.dart';
import 'package:page_view_indicators/circle_page_indicator.dart';

class VideoHeaderPageView extends StatefulWidget {

  final List<Video> videos;

  VideoHeaderPageView(this.videos);

  @override
  _VideoHeaderPageViewState createState() => _VideoHeaderPageViewState();
}

class _VideoHeaderPageViewState extends State<VideoHeaderPageView> {

  final _pageController = PageController();
  final _currentPageNotifier = ValueNotifier<int>(0);
  int _currentPosition = 0;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(FadeInRoute(widget: VideoDetails(videoId: widget.videos[_currentPosition].id,)));
      },
      child: Container(
        child: Stack(
          children: [
            PageView.builder(
              controller: _pageController,
              itemCount: widget.videos.length,
              onPageChanged: (int index) {
                setState(() {
                  _currentPosition = index;
                });
                _currentPageNotifier.value = index;
              },
              itemBuilder: (ctx, position) {
                return VideoHeaderPage(
                  video: widget.videos[position],
                );
              }
            ),
            Positioned.fill(
                child: Align(
              alignment: Alignment.bottomCenter,
              child: _tvShowInfo(context),
            ))
          ],
        ),
      ),
    );
  }

  Widget _tvShowInfo(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _ratingBar(4, context),
        SizedBox(
          height: 12,
        ),
        Text(
          widget.videos[_currentPosition].name,
          style: Theme.of(context).textTheme.subtitle1,
        ),
        SizedBox(
          height: 4,
        ),
        Text(
          'adventure, comedy',
          style: Theme.of(context).textTheme.subtitle2,
        ),
        SizedBox(
          height: 16,
        ),
        CirclePageIndicator(
          selectedDotColor: Theme.of(context).accentColor,
          dotColor: Theme.of(context).accentColor.withOpacity(0.2),
          itemCount: widget.videos.length,
          currentPageNotifier: _currentPageNotifier,
        ),
      ],
    );
  }

  Widget _ratingBar(int rating, BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.yellow,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(Radius.circular(16))),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            _ratingIcon(rating > 0, context),
            _ratingIcon(rating > 1, context),
            _ratingIcon(rating > 2, context),
            _ratingIcon(rating > 3, context),
            _ratingIcon(rating > 4, context),
          ],
        ),
      ),
    );
  }

  Widget _ratingIcon(bool isFilled, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 2),
      child: Container(
        width: 14,
        height: 14,
        child: Image.asset(
          'assets/img/star.png',
          width: 14,
          height: 14,
          fit: BoxFit.cover,
          color: isFilled
              ? Theme.of(context).primaryColor
              : Theme.of(context).primaryColor.withOpacity(0.5),
        ),
      ),
    );
  }
}
