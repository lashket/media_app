import 'package:flutter/material.dart';
import 'package:media_app/presentation/widgets/video/sections/video_section_view.dart';

import 'genres_section_view.dart';

class VideoSectionsWidget extends StatefulWidget {
  @override
  _VideoSectionsWidgetState createState() => _VideoSectionsWidgetState();
}

class _VideoSectionsWidgetState extends State<VideoSectionsWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        VideoSectionView(),
        GenresSectionView(),
        VideoSectionView(),
      ],
    );
  }
}
