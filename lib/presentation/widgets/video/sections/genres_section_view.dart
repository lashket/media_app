import 'package:flutter/material.dart';
import 'package:media_app/domain/models/tv_shows_category.dart';
import 'package:media_app/presentation/widgets/video/sections/genre_section_item_view.dart';

class GenresSectionView extends StatefulWidget {
  @override
  _GenresSectionViewState createState() => _GenresSectionViewState();
}

class _GenresSectionViewState extends State<GenresSectionView> {

  final categories = TvShowSectionCategory.availableValues();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: 15,
          itemBuilder: (context, index) {
            return GenreSectionItemView();
          },
        ),
      ),
    );
  }
}
