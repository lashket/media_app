import 'package:flutter/material.dart';
import 'package:media_app/presentation/widgets/movie_card.dart';

class VideoSectionView extends StatefulWidget {

  final isVertical;
  final String title;

  VideoSectionView({this.title = "Continue watching", this.isVertical = false});

  @override
  _VideoSectionViewState createState() => _VideoSectionViewState();
}

class _VideoSectionViewState extends State<VideoSectionView> {

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 8, bottom: 8, left: 16),
          child: Text(
            widget.title,
            style: TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.w500),
//          style: Theme.of(context).textTheme.headline1,
          ),
        ),
        Container(
          height: 220,
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: 15,
            itemBuilder: (context, index) {
              return MovieCard();
            },
          ),
        )
      ],
    );
  }


}
