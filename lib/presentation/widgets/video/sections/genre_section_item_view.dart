import 'package:flutter/material.dart';
import 'package:media_app/domain/models/tv_shows_category.dart';
import 'package:media_app/utils/constants.dart';

class GenreSectionItemView extends StatelessWidget {

  // final TvShowSectionCategory category;

  const GenreSectionItemView();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4),
      child: Container(
        height: 100,
        width: 100,
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.1),
          borderRadius: BorderRadius.circular(4.0)
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/img/ic_comedy.png', width: 50, height: 50, color: Theme.of(context).accentColor,),
            SizedBox(height: 2,),
            Text(
              'comedies',
              style: Theme.of(context).textTheme.headline2,
            ),
          ],
        ),
      ),
    );
  }
}
