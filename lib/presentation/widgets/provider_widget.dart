import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';

class ProviderWidget<T extends ChangeNotifier> extends StatefulWidget {

  final Widget Function(BuildContext context, T value, Widget child) builder;
  final Function(T) onProviderReady;
  ProviderWidget({@required this.builder, this.onProviderReady});
  @override
  _ProviderWidgetState<T> createState() => _ProviderWidgetState<T>();
}
class _ProviderWidgetState<T extends ChangeNotifier> extends State<ProviderWidget<T>> {
  T model = GetIt.I<T>();
  @override
  void initState() {
    if (widget.onProviderReady != null) {
      widget.onProviderReady(model);
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>.value(
      value: model,
      child: Consumer<T>(builder: widget.builder),
    );
  }
}