import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class ButtonView {

  factory ButtonView(TargetPlatform platform) {
    switch(platform) {
      case TargetPlatform.android: return AndroidButton();
      case TargetPlatform.iOS: return IosButton();
      default: return null;
    }
  }

  Widget build({
    @required BuildContext context,
    @required Widget child,
    @required VoidCallback onPressed
  });

}

class AndroidButton implements ButtonView {

  @override
  Widget build(
      {@required BuildContext context,
      @required Widget child,
      @required VoidCallback onPressed}) {
    return TextButton(
      child: child,
      onPressed: onPressed,
    );
  }

}

class IosButton implements ButtonView {

  @override
  Widget build(
      {@required BuildContext context,
      @required Widget child,
      @required VoidCallback onPressed}) {
    return CupertinoButton(
      child: child,
      onPressed: onPressed,
    );
  }

}