import 'package:flutter/material.dart';

class ErrorView extends StatelessWidget {

  final String errorText;
  final Function onRetry;

  const ErrorView({this.errorText = "Error occurred", this.onRetry});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              width: 100,
              height: 100,
              child: Image.asset('assets/img/ic_error.png'),
            ),
            SizedBox(height: 16,),
            Text(errorText, style: Theme.of(context).textTheme.headline1,)
          ],
        ),
      ),
    );
  }
}
