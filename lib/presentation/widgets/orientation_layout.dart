import 'package:flutter/material.dart';
import 'package:media_app/presentation/widgets/responsive_view.dart';

class OrientationLayout extends StatelessWidget {
  final Widget landscape;
  final Widget portrait;

  const OrientationLayout(
      {Key key, this.portrait, this.landscape})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if(MediaQuery.of(context).orientation == Orientation.landscape) {
      return landscape;
    }
    return portrait;
  }
}