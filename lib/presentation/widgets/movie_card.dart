import 'package:flutter/material.dart';
import 'package:media_app/navigation/routes.dart';
import 'package:media_app/presentation/screens/video_info/video_details_screen.dart';


class MovieCard extends StatelessWidget {

  final isVertical;

  const MovieCard({this.isVertical = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 120,
      child: Padding(
        padding: const EdgeInsets.only(left: 6),
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(FadeInRoute(widget: VideoDetails()));
          },
          child: Column(
            children: [
              Container(
                width: 120.0,
                height: 180,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  elevation: 2,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Image.network(
                      'https://upload.wikimedia.org/wikipedia/ru/b/be/Lost-Season1.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 4,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 6),
                child: Container(
                  alignment: Alignment(-1, -1),
                  child: Text(
                    'Stranger things',
                    overflow: TextOverflow.fade,
                    maxLines: 1,
                    softWrap: false,
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
