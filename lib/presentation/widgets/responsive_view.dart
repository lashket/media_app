import 'package:flutter/material.dart';

class ResponsiveView extends StatelessWidget {
  final Widget Function(
      BuildContext context, ScreenInformation screenInformation) builder;

  const ResponsiveView({Key key, this.builder}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screenInformation = ScreenInformation();
    return builder(context, screenInformation);
  }
}

class ScreenInformation {
  final DeviceScreenType deviceScreenType;
  final Size screenSize;
  final Size localWidgetSize;

  ScreenInformation({
    this.deviceScreenType,
    this.screenSize,
    this.localWidgetSize,
  });
}

enum DeviceScreenType { Mobile, Tablet, Desktop }

DeviceScreenType getDeviceType(MediaQueryData mediaQuery) {
  double deviceWidth = mediaQuery.size.shortestSide;

  if (deviceWidth > 950) {
    return DeviceScreenType.Desktop;
  }

  if (deviceWidth > 600) {
    return DeviceScreenType.Tablet;
  }

  return DeviceScreenType.Mobile;
}
