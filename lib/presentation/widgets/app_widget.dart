import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:media_app/navigation/destination.dart';
import 'package:media_app/presentation/screens/films/films_screen_widget.dart';
import 'package:media_app/presentation/screens/profile/profile_screen_widget.dart';
import 'package:media_app/presentation/screens/search/search_screen_widget.dart';
import 'package:media_app/presentation/screens/tv_channels/tv_channels_screen_widget.dart';
import 'package:media_app/presentation/screens/tv_shows/tv_shows_screen_widget.dart';
import 'package:media_app/utils/constants.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
          primaryColor: PRIMARY_COLOR,

          accentColor: ACCENT_COLOR,
          canvasColor: Colors.transparent,
          textTheme: TextTheme(
            headline2:  TextStyle(
                fontSize: 14,
                color: PRIMARY_TEXT_COLOR
            ),
            subtitle1: TextStyle(
              fontSize: 32,
              color: PRIMARY_TEXT_COLOR
            ),
            subtitle2: TextStyle(
              fontSize: 14,
              color: SECONDARY_TEXT_COLOR
            ),
              headline1: TextStyle(
            fontSize: 18.0,
            color: PRIMARY_TEXT_COLOR,
          ))),
      home: MainScreen(),
    );
  }
}

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>
    with TickerProviderStateMixin<MainScreen> {
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _currentIndex,
        children: <Widget>[
          TvShows(),
          FilmsScreen(),
          TvChannels(),
          SearchScreen(),
          ProfileScreen()
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          backgroundColor: Theme.of(context).primaryColor,
          selectedItemColor: Theme.of(context).accentColor,
          unselectedItemColor: PRIMARY_TEXT_COLOR.withOpacity(0.8),
          showSelectedLabels: true,
          showUnselectedLabels: true,
          type: BottomNavigationBarType.fixed,
          onTap: (int index) {
            setState(() {
              _currentIndex = index;
            });
          },
          items: allDestinations.map((Destination destination) {
            return BottomNavigationBarItem(
                icon: Icon(destination.icon), title: Text(destination.title));
          }).toList()),
    );
  }

}
