import 'package:flutter/material.dart';

class RatingIcon extends StatelessWidget {
  final bool isFilled;
  final Color filledColor;
  final double starSize;
  final EdgeInsetsGeometry padding;

  const RatingIcon(this.isFilled, this.padding,
      {this.filledColor = Colors.yellow, this.starSize = 14.0});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Container(
        width: starSize,
        height: starSize,
        child: Image.asset(
          'assets/img/star.png',
          width: starSize,
          height: starSize,
          fit: BoxFit.cover,
          color: isFilled ? filledColor : filledColor.withOpacity(0.5),
        ),
      ),
    );
  }
}
