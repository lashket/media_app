import 'dart:ui';

import 'package:flutter/material.dart';

const Color PRIMARY_TEXT_COLOR = Colors.white;

const Color SECONDARY_TEXT_COLOR = Color(0xff92989e);

const Color ACCENT_COLOR = Color(0xfff50161);

const Color PRIMARY_COLOR = Color(0xff001122);

const String BASE_URL = "http://134.209.239.212:6060/api/v1/";

const TextStyle LIST_TITLE_STYLE = TextStyle(
  fontSize: 16,
  color: PRIMARY_TEXT_COLOR,
);

const TextStyle LIST_SUBTITLE_STYLE = TextStyle(
  fontSize: 14,
  color: SECONDARY_TEXT_COLOR,
);

