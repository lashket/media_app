import 'package:media_app/data/core/Response.dart';
import 'package:media_app/domain/entities/video.dart';

abstract class TvShowsRepository {

  Future<Response<List<Video>>> fetchPosters();

  Future<Response<Video>> fetchTvShowById(int id);

}