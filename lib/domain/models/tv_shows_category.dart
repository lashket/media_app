
class TvShowSectionCategory {

  final String assetLink;
  final String name;

  const TvShowSectionCategory._internalConstructor(this.assetLink, this.name);

  static var RUSSIAN = TvShowSectionCategory._internalConstructor("ru", "Русский");
  static var ENGLISH = TvShowSectionCategory._internalConstructor("en", "English");

  static List<TvShowSectionCategory> availableValues() {
    return [
      RUSSIAN,
      ENGLISH
    ];
  }

}