import 'package:media_app/data/core/Response.dart';
import 'package:media_app/domain/entities/video.dart';

abstract class GetTvShowsPostersUseCase {

  Future<Response<List<Video>>> execute();

}