import 'package:media_app/data/core/Response.dart';
import 'package:media_app/domain/entities/video.dart';

abstract class GetTvShowByIdUseCase {

  Future<Response<Video>> fetchVideoById(int id);

}