class Season {

  final int id;
  final String name;
  final int number;
  final String description;

  Season({this.id, this.name, this.number, this.description});

}