import 'package:media_app/domain/entities/season.dart';

class Video {

  final int id;
  final String name;
  final String description;
  final String posterUrl;
  final List<Season> seasons;

  const Video({this.id, this.name, this.description, this.posterUrl, this.seasons});

}