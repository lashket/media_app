class Response<T> {

  final T data;
  final Exception exception;

  const Response({this.data, this.exception});

  bool get isSuccess  => exception == null;

}