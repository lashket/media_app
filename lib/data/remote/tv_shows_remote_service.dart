import 'dart:convert';

import 'package:get_it/get_it.dart';
import 'package:media_app/data/core/Response.dart';
import 'package:media_app/data/models/single_video_response.dart';
import 'package:media_app/data/models/tv_shows_list_response.dart';
import 'package:dio/dio.dart' as dio;
import 'package:media_app/domain/entities/season.dart';
import 'package:media_app/domain/entities/video.dart';
import 'package:media_app/utils/constants.dart';

class TvShowsRemoteService {
  final _dio = GetIt.I.get<dio.Dio>();

  Future<Response<List<Video>>> fetchPosters() async {
    try {
      final response = await _dio.get(_getUrl("tv-shows"));
      final responseDto =
          TvShowsListResponse.fromJson(jsonDecode(response.data));
      return Response(
          data: responseDto.data
              .map((e) => Video(
                  name: e.attributes.name,
                  description: e.attributes.description,
                  posterUrl: e.attributes.image,
                  id: int.parse(e.id)))
              .toList());
    } catch (e) {
      return Response(exception: Exception());
    }
  }

  Future<Response<Video>> fetchVideoById(int id) async {
    try {
      final response = await _dio.get(_getUrl("tv-shows/$id"),
          queryParameters: {"include": "seasons"});
      final singleVideoDto =
          SingleVideoResponse.fromJson(jsonDecode(response.data));
      return Response(
          data: Video(
        name: singleVideoDto.data.attributes.name,
        description: singleVideoDto.data.attributes.description,
        posterUrl: singleVideoDto.data.attributes.image,
        seasons: singleVideoDto.included
            .map((it) => Season(
                id: int.parse(it.id),
                name: it.attributes.name,
                number: it.attributes.number))
            .toList(),
      ));
    } catch (e) {
      return Response(exception: Exception());
    }
  }

  String _getUrl(String endPoint) {
    return "$BASE_URL$endPoint";
  }
}
