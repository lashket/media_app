
import 'package:json_annotation/json_annotation.dart';
part 'video_attributes.g.dart';

@JsonSerializable(explicitToJson: true)
class VideoAttributes {

  final String name;
  final String description;
  final String year;
  final String image;

  const VideoAttributes(
      this.name,
      this.description,
      this.year,
      this.image
      );

  factory VideoAttributes.fromJson(Map<String, dynamic> json) => _$VideoAttributesFromJson(json);

  Map<String, dynamic> toJson() => _$VideoAttributesToJson(this);

}