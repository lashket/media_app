
import 'package:json_annotation/json_annotation.dart';
import 'package:media_app/data/models/video_dto.dart';

part 'tv_shows_list_response.g.dart';

@JsonSerializable()
class TvShowsListResponse {

  final List<VideoDto> data;

  const TvShowsListResponse(this.data);

  factory TvShowsListResponse.fromJson(Map<String, dynamic> json) => _$TvShowsListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$TvShowsListResponseToJson(this);

}