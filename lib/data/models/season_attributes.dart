import 'package:json_annotation/json_annotation.dart';
part 'season_attributes.g.dart';

@JsonSerializable()
class SeasonAttributes {

  final int number;
  final String name;
  final String description;
  final String year;
  final String image;

  SeasonAttributes(
      this.number, this.name, this.description, this.year, this.image);

  factory SeasonAttributes.fromJson(Map<String, dynamic> json) => _$SeasonAttributesFromJson(json);

  Map<String, dynamic> toJson() => _$SeasonAttributesToJson(this);

}