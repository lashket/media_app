import 'package:json_annotation/json_annotation.dart';
import 'package:media_app/data/models/season_attributes.dart';

part 'season_dto.g.dart';

@JsonSerializable()
class SeasonDto {

  final String id;
  final SeasonAttributes attributes;

  SeasonDto(this.id, this.attributes);

  factory SeasonDto.fromJson(Map<String, dynamic> json) => _$SeasonDtoFromJson(json);

  Map<String, dynamic> toJson() => _$SeasonDtoToJson(this);

}