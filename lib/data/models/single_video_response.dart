import 'package:json_annotation/json_annotation.dart';
import 'package:media_app/data/models/season_dto.dart';
import 'package:media_app/data/models/video_dto.dart';

part 'single_video_response.g.dart';

@JsonSerializable()
class SingleVideoResponse {

  final VideoDto data;
  final List<SeasonDto> included;

  SingleVideoResponse(this.data,this.included);

  factory SingleVideoResponse.fromJson(Map<String, dynamic> json) => _$SingleVideoResponseFromJson(json);

  Map<String, dynamic> toJson() => _$SingleVideoResponseToJson(this);

}