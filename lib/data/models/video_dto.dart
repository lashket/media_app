
import 'package:json_annotation/json_annotation.dart';
import 'package:media_app/data/models/video_attributes.dart';

part 'video_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class VideoDto {

  final VideoAttributes attributes;
  final String id;

  const VideoDto(this.attributes, this.id);

  factory VideoDto.fromJson(Map<String, dynamic> json) => _$VideoDtoFromJson(json);

  Map<String, dynamic> toJson() => _$VideoDtoToJson(this);

}