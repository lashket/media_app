import 'package:flutter/cupertino.dart';
import 'package:get_it/get_it.dart';
import 'package:media_app/data/core/Response.dart';
import 'package:media_app/domain/entities/video.dart';
import 'package:media_app/domain/repository/tv_shows_repository.dart';
import 'package:media_app/domain/usecases/tv_shows/get_tv_shows_posters_use_case.dart';

class GetTvShowsPostersUseCaseImpl implements GetTvShowsPostersUseCase {

  TvShowsRepository repository = GetIt.I.get<TvShowsRepository>();

  @override
  Future<Response<List<Video>>> execute() async =>  await repository.fetchPosters();

}