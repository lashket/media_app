import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get_it/get_it.dart';
import 'package:media_app/data/core/Response.dart';
import 'package:media_app/data/models/tv_shows_list_response.dart';
import 'package:media_app/data/remote/tv_shows_remote_service.dart';
import 'package:media_app/domain/entities/video.dart';
import 'package:media_app/domain/repository/tv_shows_repository.dart';
import 'package:dio/dio.dart' as dio;
import 'package:http/http.dart' as http;

class TvShowsRepositoryImpl implements TvShowsRepository {
  final TvShowsRemoteService apiService =
      GetIt.instance.get<TvShowsRemoteService>();

  @override
  Future<Response<List<Video>>> fetchPosters() async => apiService.fetchPosters();

  @override
  Future<Response<Video>> fetchTvShowById(int id) async =>apiService.fetchVideoById(id);

}
