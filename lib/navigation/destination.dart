import 'package:flutter/material.dart';
import 'package:media_app/utils/constants.dart';

class Destination {

  const Destination(this.title, this.icon, {this.color = ACCENT_COLOR});

  final String title;
  final IconData icon;
  final Color color;

}

const List<Destination> allDestinations = <Destination>[
  Destination('TV-Shows', Icons.movie),
  Destination('Films', Icons.movie_outlined),
  Destination('TV', Icons.tv),
  Destination('Search', Icons.search),
  Destination('Profile', Icons.person),
];